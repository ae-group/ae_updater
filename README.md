<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# updater 0.3.15

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_updater/develop?logo=python)](
    https://gitlab.com/ae-group/ae_updater)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_updater/release0.3.14?logo=python)](
    https://gitlab.com/ae-group/ae_updater/-/tree/release0.3.14)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_updater)](
    https://pypi.org/project/ae-updater/#history)

>ae_updater module 0.3.15.

[![Coverage](https://ae-group.gitlab.io/ae_updater/coverage.svg)](
    https://ae-group.gitlab.io/ae_updater/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_updater/mypy.svg)](
    https://ae-group.gitlab.io/ae_updater/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_updater/pylint.svg)](
    https://ae-group.gitlab.io/ae_updater/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_updater)](
    https://gitlab.com/ae-group/ae_updater/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_updater)](
    https://gitlab.com/ae-group/ae_updater/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_updater)](
    https://gitlab.com/ae-group/ae_updater/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_updater)](
    https://pypi.org/project/ae-updater/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_updater)](
    https://gitlab.com/ae-group/ae_updater/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_updater)](
    https://libraries.io/pypi/ae-updater)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_updater)](
    https://pypi.org/project/ae-updater/#files)


## installation


execute the following command to install the
ae.updater module
in the currently active virtual environment:
 
```shell script
pip install ae-updater
```

if you want to contribute to this portion then first fork
[the ae_updater repository at GitLab](
https://gitlab.com/ae-group/ae_updater "ae.updater code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_updater):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_updater/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.updater.html
"ae_updater documentation").
